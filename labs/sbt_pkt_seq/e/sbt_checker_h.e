<'
-- Performs data checking using a scoreboard technique (keeping track of 
-- things on a list).  This assures packets are correctly routed to the 
-- correct port without being corrupted.
unit sbt_checker_u {   
  -- Used to keep track of data-item instance as they are sent to 
  -- the DUT.  Items will be removed from this list as they are 
  -- seen at one of the DUT output ports.
  !scoreboard: list of sbt_packet_s;

  -- "Hook" from the monitor.  This should be bind'ed to the appropriate
  -- monitor(s) by the enclosing unit.
  got_pkt_imp:  in method_port of pkt_pipe is instance;
  keep bind(got_pkt_imp, empty);

  -- Empty local method corresponding to in method_port. To be implemented later.
  got_pkt_imp(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is { };

  -- Empty local method to log incoming packets on the scoreboard.  And also
  -- will assure that the outgoing packets are not corrupted and
  -- were routed to the correct output port. Notice the arguments 
  -- passed to this method (when called) include enough information to 
  -- allow the checker to "know" from which monitor the packet came.
  got_pkt(pkt: sbt_packet_s, name: sbt_env_name_t, port_num: byte) is {};

};

'>
