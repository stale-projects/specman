<'
import sbt_pkt_seq_top.e;
#ifndef SPECMAN_VERSION_6_2_OR_LATER {
  #ifdef SPECMAN_VERSION_6_1_OR_LATER {
    import patches/sbt_seq_patch1.e;
  };
};
 
-- Top of the testbench hierarchy.  sbt_env_u is instanced here.
extend sys {
   -- This is the sbt_pkt_seq package top-level env.
  env: sbt_env_u is instance;
  keep env.agents.size() == 3;  
  keep env.agent.active_passive == ACTIVE;
  keep env.hdl_path() == "~/top";
};
'>

