<'
import vt_util/e/vt_util_top;
extend MAIN sbt_seq_s {
  !seq: sbt_seq_s;
  body() @driver.clock is only {
    for i from 1 to 10 {
      do ALL_BIG seq;
    };
  };
};

-- parent sequence
extend sbt_seq_kind_t: [ALL_BIG];
extend ALL_BIG sbt_seq_s {
  !seq: ATOMIC sbt_seq_s;
  body() @driver.clock is only {
    do seq keeping { 
      .consecutive == 5;
      .pkt.payload_size == LARGE;
    };
  };
};

-- sub-sequence being generated
extend sbt_seq_kind_t: [ATOMIC];
extend ATOMIC sbt_seq_s {
  !pkt: sbt_packet_s;
  consecutive: byte [1..10];
  body() @driver.clock is only {
    grab(driver);
    for i from 1 to consecutive {
      do pkt;
    }; 
    ungrab(driver);
  };
};


--------------------------------------------
extend sbt_driver_u {
  gen_10_pkts() @clock is only {
    for i from 1 to 100 {
      pkt = get_next_item();
      pkt_history.push(pkt);
      emit item_done;
    };
  };
};
extend MAIN sbt_seq_s {keep count > 100};

'>
