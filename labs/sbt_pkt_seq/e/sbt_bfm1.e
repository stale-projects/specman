<'
import sbt_bfm1_h;
extend sbt_bfm1_u {

  -- We will be setting these references procedurally; 
  --  so prefix each with "!".
  !data_iop    : inout simple_port of byte;
  !valid_iop   : inout simple_port of bit;
  !suspend_iop : inout simple_port of bit;
  
  some_delay: byte; keep soft some_delay in [1..5];

  run() is also {
     valid_iop$ = 0;
  };
  drive_bus() @clock_e is only {
     var pkt: sbt_packet_s;
     wait [some_delay];
     while TRUE {
       pkt = get_pkt();
       send_pkt(pkt);
       its_done();
       wait [pkt.pkt_delay];
     };
  };

  send_pkt(in_pkt: sbt_packet_s) @clock_e is only {
    var lob: list of byte;
    lob = pack(packing.low, in_pkt);
    for each (pkt_byte) in lob {
      sync true(suspend_iop$ == 0);
      data_iop$  = pkt_byte;
      valid_iop$ = 1;
      if (lob.size() == index+1) {
        valid_iop$ = 0;   
      };
      wait [1];
    };
  };
};
'>
