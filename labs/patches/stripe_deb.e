<'

//This file obtained from Orit Greengrass (orit@cadence.com).
//A patch to work around a known Specman-issue when using 
//"trace seq" or "wave seq" commands
//See related SR issue: 40812724 , related PCRs# 4029153 & 4028932

extend any_sequence_item {
    
 get_stripe_suffix(s : vt_stripe) is only{
      var p_lnk : string;
      var m_name : string;      // module name
      var m_line : uint;
      var source : string;
      var do_line : string;
           
       // p_link
      if parent_sequence != NULL {
         s.add_divider();  
         p_lnk = vt.nlnk_novis_instance(append(stripe_info.idx,"_parent"),
          me.parent_sequence,append("Parent: @",
           str_split(vt.instance_to_string(me.parent_sequence),"@").top()),"");
          if stripe_info.get_do_location() ~  "/^at line (\d+) in @(\w+)$/" {
            m_name = $2;
            m_line = $1.as_a(int);
         }; // if s.do_locatio...
         
          if stripe_info.get_do_location() ~ "/^at line (\d+) in @(\w+)$/" then {
            var m_line:= $1.as_a(int);
            var fname:= symtab.modules.key($2).full_file_name;
            var fc:= files.get_file_contents(fname, FALSE);
            if ( fc != NULL) {
                do_line = fc.lines[m_line - 1];
            };
         };
         
         if do_line != "" {
            source = vt.nlnk_command(append(stripe_info.idx,"_source"), // link name
             append("source @",m_name," ",m_line),
             str_chop(str_replace(do_line, "/^\s+/" , ""),20),"");
         } else {
            source = "No Source";
         };
                  
         s.add_box(p_lnk , source ,LIGHT_GRAY);
      };
   };
   
};  

'>
