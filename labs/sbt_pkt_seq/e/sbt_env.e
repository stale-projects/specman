<'
import sbt_env_h;
extend sbt_env_u {
     -- To the synchronizer, a reset port has been added.
     event reset_start_e is rise(sync.reset_iop$)@sim;
     event reset_end_e   is fall(sync.reset_iop$)@sim;

     -- Add an instance of the reset-irritator/BFM to the environment:
     reset_bfm: sbt_reset_bfm_u is instance;

     !reset_on:  bool;

     on reset_start_e {
       reset_on = TRUE;
       message(LOW, "++++++++ RESET START ++++++++");
       if (get_objection_counter(TEST_DONE) == 0) {
          -- This env unit which does not get "rerun()" during reset, must 
          -- log an objection until its children units are re-activated 
          -- after being reset.
          raise_objection(TEST_DONE);
       };
       checker.rerun();
     };
     on reset_end_e {
       reset_on = FALSE;
       message(LOW, "-------- RESET END   --------");
       start timer();
     };
     reset_delay: uint;
     keep soft reset_delay == 1000;
     timer() @sync.clk_ip$ is {
        wait [reset_delay];
        if (get_objection_counter(TEST_DONE) == 1) {
           drop_objection(TEST_DONE);
        };
     };

    connect_pointers() is also {
      reset_bfm.parent = me;
      reset_bfm.reset_iop = sync.reset_iop;
    };

    -- Extend the connect_ports() method here (use is "is also"),
    -- and procedurally bind (use do_bind()) the agent's monitor's 
    -- "out_pkt_omp()" method_port to
    -- the checker instance's "got_pkt_imp()" 
    -- method_port.
    connect_ports() is also {
      do_bind(agent.mon.out_pkt_omp, checker.got_pkt_imp);
      for each (a) in agents {
        do_bind(a.mon.out_pkt_omp, checker.got_pkt_imp);
      };
    };

};

extend sbt_reset_bfm_u {
  !parent: sbt_env_u;
  event raw_clock_e is only @parent.sync.clk_ip$;
};

extend sbt_agent_u {
   -- Here you should "connect" the clock event_port 
   -- instanced in the synchronizer to the 'main_clk_e'
   -- event in the agent.  
   -- Use "event main_clk_e is only ..." to do this.
   event reset_start_e is only cycle @parent.reset_start_e;
   event main_clk_e    is only true(!parent.reset_on)@parent.sync.clk_ip$;
}; 
'>
