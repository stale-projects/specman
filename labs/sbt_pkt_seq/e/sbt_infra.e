<'
-- Sequence definition for driving the SBT router DUT input interface.
sequence sbt_seq_s using
  item           = sbt_packet_s,
  created_driver = sbt_driver_u,
  created_kind   = sbt_seq_kind_t;

extend sbt_seq_s {
  -- As a convenience to users of this sequence, this field gets 
  -- assigned to the absolute maximum value that a packet address can be.
  !max_addr: byte;
  pre_generate() is also {
    var sample_pkt: sbt_packet_s;
    gen sample_pkt;
    max_addr = sample_pkt.max_addr;
  };
};

extend ACTIVE ROUTER_IN sbt_agent_u {
  -- Instance of the sbt_seq_s sequence driver.
  driver: sbt_driver_u is instance;
  keep driver.agent == me;
  keep bfm.driver == driver;
  rerun() is also {
    driver.rerun();
  };
};

extend sbt_driver_u {
  -- Reference to the driver's enclosing unit.
  agent: sbt_agent_u;
  -- Connected to the agent's main_clk_e which should be triggered by the DUT clock.
  event clock is only @agent.main_clk_e;
};

extend sbt_bfm1_u {	
  -- Reference to the sbt_seq_s sequence driver.
  driver : sbt_driver_u;

  -- Runs forever. Gets as many sbt_packet_s data-items as seq driver will provide.
  get_pkt(): sbt_packet_s @clock_e is also {
    result = driver.get_next_item();
  };

  -- Emits item_done for the sequence driver to know the BFM is finished sending in the pkt.
  its_done() is also {
    emit driver.item_done;
  };
};
'>

