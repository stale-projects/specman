<'
-- Base-class unit that will be "like-inherited" by 
-- units in this package.  Provides useful fields for
-- naming information kept by agents, BFMs and monitors.
unit sbt_phy_u {
   -- To allow BFMs and monitors to identify themselves
   -- as to which interace they are attached.  
   port_name: sbt_env_name_t;
   
   -- To allow BFMs and monitors to identify themselves
   -- as to which port-number they are attached.  
   port_num : byte;
   keep soft port_num == 0;

   post_generate() is also {
     message(LOW, me, " created");                       
   };


};
'>
