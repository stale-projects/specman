<'
import sbt_sig_h;
extend sbt_sig_u {

  data_iop   : inout simple_port of byte is instance;
  valid_iop  : inout simple_port of bit  is instance;
  suspend_iop: inout simple_port of bit  is instance;
  error_iop  : inout simple_port of bit  is instance;  

  keep bind(data_iop   , external);
  keep bind(valid_iop  , external);
  keep bind(suspend_iop, external);
  keep bind(error_iop  , empty);  --overidden as an external port for ROUTER_IN

  when ROUTER_IN sbt_sig_u {
    keep soft data_iop.hdl_path()    == "data";
    keep soft valid_iop.hdl_path()   == "packet_valid";
    keep soft suspend_iop.hdl_path() == "suspend_data_in";
    keep soft error_iop.hdl_path()   == "err";
    keep bind(error_iop  , external);
  };
  when ROUTER_OUT sbt_sig_u {
    keep soft data_iop.hdl_path()    == append("channel"  , dec(port_num));
    keep soft valid_iop.hdl_path()   == append("vld_chan_", dec(port_num)); 
    keep soft suspend_iop.hdl_path() == append("suspend_" , dec(port_num));
  };
};
'>
