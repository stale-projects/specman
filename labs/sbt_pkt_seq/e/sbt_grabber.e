<'
import vt_util/e/vt_util_top;
extend MAIN sbt_seq_s {
  !sl: SHORT_LONG sbt_seq_s;
  !mm: MIN_MAX    sbt_seq_s; 
  !seq:           sbt_seq_s;
  !p :            sbt_packet_s; 
  body() @driver.clock is only {
    all of {
      {  for i from 1 to 10 {
           do p; do p; do SMALL p; do ALL_BAD seq;
           wait [1]; 
         };
      };
      {  
 --      grab(driver);
 --      do mm; 
 --      ungrab(driver);
      };
    };
  };
};
------------------------------------------
extend sbt_seq_kind_t: [INT];
extend INT sbt_seq_s {
  !mm: MIN_MAX sbt_seq_s;
  body() @driver.clock is only {
    wait [1];
    grab(driver);
    do mm;
    ungrab(driver);
  };
};
-------------------------------------------
extend sbt_driver_u {
  int_seq: INT sbt_seq_s;
  keep int_seq.driver == me ;
  run() is also {
    int_seq.start_sequence();
  };
};
--------------------------------------------
extend sbt_driver_u {
  gen_10_pkts() @clock is only {
    for i from 1 to 100 {
      pkt = get_next_item();
      pkt_history.push(pkt);
      emit item_done;
    };
  };
};
extend MAIN sbt_seq_s {keep count > 100};

'>
