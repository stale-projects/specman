*---------------------------------------------------
File name   : sbt_pkt_seq_top.e
Title       : Top level of sbt_pkt_seq eVC
Project     : Specman Basic Training
Developers  : Kevin Schott (Correct Designs, Inc.)
Created     : 20-Nov-2005
Description : This file imports all files in the eVC
Notes       : Files ending in _h.e are public files
            : all others files could be encrypted.
----------------------------------------------------*/
<'
package sbt_pkt_seq;
define SBT_PKT_SEQ_VERSION_1_0;

import evc_util/e/evc_util_top;
//import vt_util/e/vt_util_top; //don't need for 8.1

--Import common building-block and base-class definitions
import sbt_types;
import sbt_packet;
import sbt_method_types;
import sbt_phy;

--Import all the 'h' files first to avoid any import ordering issues
--where one unit references another unit
import sbt_mon_h;
import sbt_bfm1_h;
import sbt_bfm2_h;
import sbt_sig_h;
import sbt_agent_h;
import sbt_sync_h;
import sbt_checker_h;
import sbt_env_h;

import sbt_reset_bfm;

--Now import the "implementation" of each of 
--the Verification Environment (VE) components 
import sbt_mon;
import sbt_bfm1;
import sbt_bfm2;
import sbt_sig;
import sbt_agent;
import sbt_sync;
import sbt_checker;
import sbt_env;

import sbt_infra;   

--A test case may choose to import the following file
--after all of the above files are imported:
--import sbt_lib.e
'>
