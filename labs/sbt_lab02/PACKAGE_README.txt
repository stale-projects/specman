* Title: Cadence SBT Stimulus Creation: Structs Fields and Constraints
* Name: sbt_lab02  
* Version: 8.1
* Requires:
  specman {5.0.2,5.0.3,5.1,6.0,6.1,6.2,8.1 ..}
* Modified: Jan-2009
* Category: Specman Basic Training eRM Lab 
* Support: specman_user@ilovespecman.com
* Documentation: docs/sbt_lab02.txt
* Release notes: docs/sbt_lab02_release_notes.txt
* Description:
    This is a Specman Basic Training v8.1 lab 2. 

