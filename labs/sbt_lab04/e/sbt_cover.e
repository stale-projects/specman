<'
extend sbt_packet_s {
  event pkt_gen_e;
  cover pkt_gen_e is {
    item pkt_size;
    item pkt_kind;
    item version;
  };
  post_generate() is also {
    emit pkt_gen_e;
  };
};
'>
