* Title: Cadence SBT Packet Sequence eVC
* Name: sbt_pkt_seq
* Version: 8.1
* Requires:
  specman {5.0.2,5.0.3,5.1,6.0,6.1,6.2,8.1 ..}
* Modified: Oct-2008  
* Category: Golden eVC
* Support: support@cadence.com
* Documentation: docs/sbt_pkt_seq.pdf
* Release notes: docs/sbt_pkt_seq_release_notes.txt
* Description:
    This is the Specman Basic Training Sequence eVC.  It demonstrates how
to use sequences using the Specman Basic Training Router DUT. 

