* Title: Cadence SBT Intro Lab: Running Specman Elite
* Name: sbt_lab01  
* Version: 8.1
* Requires:
  specman {5.0.2,5.0.3,5.1,6.0,6.1,6.2,8.1 ..}
* Modified: Jan-2009
* Category: Specman Basic Training eRM Lab 
* Support: specman_user@ilovespecman.com
* Documentation: docs/sbt_lab01.txt
* Release notes: docs/sbt_lab01_release_notes.txt
* Description:
    This is a Specman Basic Training v8.1 lab 1. 

