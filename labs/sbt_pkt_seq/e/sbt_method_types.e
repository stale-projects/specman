<'
-- Used to have the monitor report activity on an interface to 
-- the router data-checker.  The monitor will detect an instance
-- of the sbt_packet_s on the bus, and report the information to 
-- the checker along with which port_name and port_number to which 
-- the monitor is attached. E.g. ROUTER_OUT-0, or ROUTER_OUT-1, etc.
method_type pkt_pipe(pkt: sbt_packet_s, port_name: sbt_env_name_t, port_num: byte);
'>
