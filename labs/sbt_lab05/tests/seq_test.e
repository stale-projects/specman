<'
import sbt_pkt_seq/e/sbt_config1;

extend MAIN sbt_seq_s {
  keep count in [5..10];  
  post_generate() is also {
    message(LOW, me, " ",  count);
  }; 
};
'>
