<'
-- This unit will drive the suspend_X interface signals of the router DUT.
unit sbt_bfm2_u like sbt_phy_u {
  -- Connected to the clock of the physical interface of the DUT.
  event clock_e;                           

  -- Runs forever. Drives the DUT signal to enable it to send out pkt bytes.
  drive_bus() @clock_e is {};

  run() is also {
    start drive_bus();
  };
};
'>
