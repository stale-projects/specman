<'
extend sbt_packet_s {

  event record_e;
  cover record_e is {
    -- add coverage items here according the lab-book instructions
  };

  post_generate() is also {
    --This event matches the cover group name that you have added
    emit record_e;

    --This event matches the cover group name that is coded for you in the
    --eVC file: sbt_pkt_seq/e/sbt_packet.e
    emit detected_e;
  };

};
'>
