<'
import evc_util/e/evc_util_top;
type sbt_pkt_size_t: [SHORT, LONG, WHO_CARES];
struct sbt_packet_s {
    pkt_size: sbt_pkt_size_t;
    addr  : uint(bits:2); 
    len   : uint(bits:6); 
    data [len]: list of byte;
    parity: byte;

    keep pkt_size == SHORT  => len < 10;
    keep pkt_size == LONG   => len > 20;

    keep len  != 0;
    keep addr != 3;
    
    keep parity == parity_calc(addr, len, data);

    parity_calc(in_addr: uint(bits:2), in_len:uint(bits:6), in_data: list of byte): byte is {
      result = %{in_len, in_addr};
      for each (d) in in_data {
        result = result ^ d;
      };
    };
    
    post_generate() is also {
        messagef(LOW,"Sending to %x a packet of size %x with length %x \n", addr, pkt_size, len );
    };
};
'>
