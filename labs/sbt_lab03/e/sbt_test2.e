<'
import sbt_config.e;
--Modify the properties of sys by extending it
--and add fields and constraints      
extend sys {
    keep packets.size() == 20;
    keep for each (packet) in packets {
        (index < 5) => packet.addr == 0;
        ((5  <= index) and (index < 10)) => packet.addr == 1;
        ((10 <= index) and (index < 15)) => packet.addr == 2;
    };
    
    !one_big_packet: sbt_packet_s;
    
    run() is also {
        gen one_big_packet keeping {
            it.pkt_size == LONG;
            it.addr == 1;
        };
    };
};
    
extend sbt_packet_s {
   keep soft pkt_size == select {
        20 : SHORT;
        80 : LONG;
    };
};
'>
