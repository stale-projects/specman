* Title: Cadence SBT Lab: Extensions with when-inheritance
* Name: sbt_lab04  
* Version: 8.1
* Requires:
  specman {5.0.2,5.0.3,5.1,6.0,6.1,6.2,8.1 ..}
* Modified: Jan-2009
* Category: Specman Basic Training eRM Lab 
* Support: specman_user@ilovespecman.com
* Documentation: docs/sbt_lab04.txt
* Release notes: docs/sbt_lab04_release_notes.txt
* Description:
    This is a Specman Basic Training v8.1 lab 4. 

