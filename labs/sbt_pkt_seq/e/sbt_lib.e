<' 

extend sbt_seq_s {
  !a_pkt: sbt_packet_s;
  -- ASSUME THAT THE ENVIRONMENT DEVELOPER HAS ADDED A NEW
  -- FIELD FOR YOU IN (THIS) sbt_seq_s STRUCT CALLED "max_addr".  
  -- ASSUME THAT THE VALUE OF THIS FIELD IS GUARANTEED TO BE SET
  -- TO "2". YOU CAN SPECIFY CONSTRAINTS IN THIS LAB THAT USE
  -- THIS FIELD.  NOTE HOW THE "max_addr" FIELD IS USED BELOW 
  -- IN SOME OF THE SEQUENCE-LIBRARY EXAMPLES.  THIS MAY BE 
  -- DIFFERENT THAN THE WAY THESE SEQUENCES WERE SHOWN IN 
  -- THE CLASS SLIDES, BUT THE EFFECT IS THE SAME FOR A 3-PORT ROUTER. 
  -- BEFORE WRITING YOUR OWN "BIG_BAD_ETC" SEQUENCE, TAKE 
  -- NOTE OF HOW THE "max_addr" IS USED TO INDIRECTLY CONSTRAIN 
  -- THE "addr" FIELD OF PACKETS BEING CREATED BY THE "do" ACTION.
};

extend sbt_seq_kind_t: [SHORT_LONG];
extend SHORT_LONG sbt_seq_s {
  // parameters
  dest: byte; 
  keep dest <= max_addr;
  //behavior
  body() @driver.clock is only {
    do a_pkt keeping {.pkt_size == SHORT && .addr == dest};
    do a_pkt keeping {.pkt_size == LONG  && .addr == dest};
  };
};

extend sbt_seq_kind_t: [ALL_BAD];
extend ALL_BAD sbt_seq_s {
   bad_count: byte [1..10];
   body() @driver.clock is only {for i from 1 to bad_count {
         do a_pkt keeping { 
            .pkt_kind == BAD;
         };
      };
   };
};


extend sbt_pkt_size_t: [MIN, MAX];
extend MIN'pkt_size sbt_packet_s {
   keep len ==  1;
};
extend MAX'pkt_size sbt_packet_s {
   keep len == 63;
};

extend sbt_seq_kind_t: [MIN_MAX];
extend MIN_MAX sbt_seq_s {
   some_delay: byte;
   keep soft some_delay in [1..5];
   body() @driver.clock is only {
      do MIN a_pkt; 
      wait [some_delay];
      do MAX a_pkt;
   };
};
 
extend sbt_seq_kind_t: [ALTERNATING];
extend ALTERNATING sbt_seq_s {
  !mm_seq: MIN_MAX sbt_seq_s;
  alt_count: uint(bits:4);	
  alt_dest: byte; 
  keep alt_dest <= max_addr;
  body() @driver.clock is only {
    for i from 1 to alt_count {
      do a_pkt keeping {
        .pkt_kind == (odd(i) ? BAD : GOOD);
        .addr     == alt_dest;
      };
    };
    do mm_seq keeping {soft .some_delay == 1};
  };
};

'>

extend sbt_seq_kind_t: [INTERLEAVE];
extend INTERLEAVE sbt_seq_s {
  !long_ac: ALTERNATING sbt_seq_s;
  !all_bad: ALL_BAD     sbt_seq_s;

  body() @driver.clock is only {
    all of {
      {do long_ac keeping {.alt_count == 10};};
      {do all_bad keeping {.bad_count == 10};};
    };
  };
};

