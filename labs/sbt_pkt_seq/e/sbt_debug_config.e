<'
import evc_util/e/evc_util_top;
import vt_util/e/vt_util_top;
import sbt_types;
import sbt_packet;

-- Sequence definition for driving the SBT router DUT input interface.
sequence sbt_seq_s using
  item           = sbt_packet_s,
  created_driver = sbt_driver_u,
  created_kind   = sbt_seq_kind_t;

extend sbt_seq_s {
  -- As a convenience to users of this sequence, this field gets
  -- assigned to the absolute maximum value that a packet address can be.
  !max_addr: byte;
  pre_generate() is also {
    var sample_pkt: sbt_packet_s;
    gen sample_pkt;
    max_addr = sample_pkt.max_addr;
  };
};

extend sys {
  driver: sbt_driver_u is instance;
};

extend sbt_driver_u {	
  -- Number of packet-items to be sent to the "BFM"
  num_of_pkts: uint;
  keep soft num_of_pkts == 500;
  -- Number of Specman ticks before test ends
  num_of_clks: uint;
  keep soft num_of_clks == 9999;

  -- holder for item being returned by get_next_item()
  !pkt:  sbt_packet_s; 
  -- Holds a list of the pkts returned by the sbt_seq_s sequences for debug.
  !pkt_history:  list of sbt_packet_s;
  -- Calls get_next_item() 10 times to see what items that the various sequences 
  -- will produce (as a result of the body() method of the MAIN sbt_seq_s).
  gen_some_pkts() @clock is {
    for i from 1 to num_of_pkts {
      pkt = get_next_item();
      pkt_history.push(pkt); 
      emit item_done;
    };
  };
  run() is also { 
     start gen_some_pkts();
     start fake_clk();
  };
  -- Used to cause the driver's clock to tick without having a simulator attached.
  fake_clk() @sys.any is {
    for i from 1 to num_of_clks {
      emit clock;
      wait [1];
    };
    stop_run();
  };
};
'>
