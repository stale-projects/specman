#!/bin/csh -f

echo "If you wish to use Synopsys' DVE:"
echo "  set the environment variable 'SYN_VCS_DVE', and re-run this script."
echo "If you wish to use vcsi:"
echo "  set the environment variable 'SYN_VCSI', and re-run this script."
set vcs_cmd="vcs"
set options=""
set debug=""
if (${?SYN_VCSI}) then
  set vcs_cmd="vcsi"
endif 
if (${?SYN_VCS_DVE}) then
  set vcs_cmd="vcsi"
  set options="-gui"
  set debug="-debug"
endif 

set spec='specview'
set lang=""

if ( `echo $0 | grep _verilog_` != "" ) then
  set lang='verilog'
  set stub='specman.v'
endif

if ( `echo $0 | grep _vhdl_`    != "" ) then
  echo "Vhdl not support for vcs for this lab"
  exit 1;
endif

echo "Using language=$lang"

# ====================================================
# Script for Specman training lab with vcs       
# ====================================================

setenv DUT ../../dut/$lang

if ( ! -e $DUT/$stub ) then 
  echo "Looks like you did not create the stubs file... exiting."
  exit 1
endif 

if ( $lang == verilog ) then
  sn_compile.sh -sim $vcs_cmd -vcs_flags "$debug -I ${DUT}/specman.v ${DUT}/router.v"
  $spec $vcs_cmd -Mupdate -o ${vcs_cmd}_specman $options -RIG ${DUT}/specman.v ${DUT}/router.v
fi 

