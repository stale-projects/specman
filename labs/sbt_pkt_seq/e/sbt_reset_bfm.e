<'
-- Random reset irritator
unit sbt_reset_bfm_u {
     -- Should be connected to the unqualifed/raw clock that always "ticks"
     event raw_clock_e;

     -- In the env, this gets connected to the reset_iop instance found in the synchronizer
     !reset_iop: inout simple_port of bit;

     -- Amount of time to elapse between resets
     reset_interval: uint;
     keep soft reset_interval == MAX_INT;

     -- The maximum number of resets that will be created per test 
     max_number_of_resets_per_test: byte;
     keep soft max_number_of_resets_per_test == 3;

     -- The maximum duration of reset    
     duration_of_reset: byte;
     keep soft duration_of_reset in [4..16];

     random_reset() @raw_clock_e is {
       var reset_count: uint;
       message(LOW, "Asserting reset in ", dec(reset_interval), " cycles.");
       while TRUE {
         wait [reset_interval];

         reset_iop$ = 1; -- Assert RESET
         reset_count = reset_count + 1;
         gen   duration_of_reset;
         wait [duration_of_reset];
         reset_iop$ = 0; -- De-assert RESET

         if (reset_count >= max_number_of_resets_per_test) {
           message(LOW, "No more resets; max number of resets has been reached");
           break;
         };
         gen reset_interval;                                             
         message(LOW, "Asserting reset in ", dec(reset_interval), " cycles.");
       };
     };

     -- Start the random reset irritator TCM
     run() is also {start random_reset(); };
};
'>
