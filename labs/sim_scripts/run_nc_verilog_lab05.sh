#!/bin/csh -f

set spec='specview -simvision'
set gui='-gui'
set lang=""
set force=`echo "$*" | grep 'force'`
set nocomp=`echo "$*" | grep 'nocomp'`
set ncsim=`echo "$*" | grep 'ncsim'`

if ( `echo $0 | grep _verilog_` != "" ) then
  set lang='verilog'
  set stub='specman.v'
  set opt='ncvlog'
  set ncfile='ncvlog_specman'
  set files="./ncelab_specman ./ncvlog_specman"
endif

if ( `echo $0 | grep _vhdl_`    != "" ) then
  set lang='vhdl'
  set stub='specman_nc.vhd'
  set opt='ncvhdl'
  set ncfile='ncvhdl_specman'
  set files='./ncvhdl_specman'
endif

if ("$ncsim" != "") then
  set opt='ncsim'
  set ncfile='ncsim_specman'
  set files='./ncsim_specman'
endif 

echo "Using language=$lang"

# ====================================================
# Script for Specman training lab with ncSim     
# ====================================================

setenv DUT ../../dut/$lang
set CDS_INST_DIR=`cds_root ncprep`
if ( ${CDS_INST_DIR} == "" ) then
  echo "Cannot find cds_root command in your PATH. This is expected to be set up already when using nc-sim"
  exit 1
endif

### Some specman installations may not have been done with a linked simulator.
### Let's just make the required exectuable and place it in the $DUT directory
if ( (! -x "${DUT}/$ncfile" || ("$force" != "") ) && ("$nocomp" == "")) then
  echo "Creating $files ..."
  sn_compile.sh -sim $opt   
  /bin/mv $files ${DUT}/.
endif 

if ( ! -e $DUT/$stub ) then 
  echo "Looks like you did not create the stubs file... exiting."
  exit 1
endif 

if ("$nocomp" == "") then
  set nc_path="${DUT}/"
else 
  set nc_path=""   
endif 

if ( $lang == vhdl ) then
  ### Make a default worklib
  if (! -e worklib) mkdir worklib
  echo 'softinclude ${CDS_INST_DIR}/tools/inca/files/cds.lib'  > cds.lib
  echo 'define worklib ./worklib'                             >> cds.lib
  echo "define WORK worklib"          > hdl.var
  echo "define NCELABOPTS -messages" >> hdl.var
  ncvhdl -messages ${DUT}/$stub ${DUT}/router.vhd  -cdslib ./cds.lib
  ncelab -messages WORKLIB.TOP:BEHAVE -access +rwc 
  $spec ${nc_path}${ncfile} -tcl $gui WORKLIB.TOP:BEHAVE
endif

if ( $lang == verilog ) then
  ncprep +overwrite ${DUT}/router.v ${DUT}/specman.v
  ncvlog ${DUT}/$stub ${DUT}/router.v -message -WORK worklib
  ${nc_path}ncelab_specman -message -access +wrc top specman specman_wave
  $spec ${nc_path}${ncfile} -NBASYNC $gui -tcl worklib.top
endif

