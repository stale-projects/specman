<'
-- Instance of a unit that will be associated with an HDL interface.
-- This unit will instance a BFM, a monitor, and a sequence driver.
unit sbt_agent_u like sbt_phy_u {
  -- Standard usage per the eRM convention.
  name: string;
  keep name == append(port_name, port_num);
  -- Used by the message()/messagef() actions to 
  -- indicate which unit displayed the message
  short_name(): string is also {
    result = name.to_string();
  };

  -- Field of a eRM pre-defined type that 
  -- indicates if this agent's BFM is actively
  -- driving the interface to which it is bound.
  -- Choices are ACTIVE and PASSIVE. Defaults to ACTIVE.
  active_passive: erm_active_passive_t; 
  keep soft active_passive == ACTIVE;

  -- Should be connected to a DUT clock
  event main_clk_e;

  -- This is the signal_map unit.
  sig: sbt_sig_u is instance;

  -- This unit will only passively sample the signals from the router DUT.
  -- It will collect interface activity and create a packet instance to 
  -- provide to the data checker. It will also collect coverage information
  -- and perform temporal checks on the interface to which it is associated.
  mon: sbt_mon_u is instance;

  keep all of {
    sig.port_name  == read_only(me.port_name);
    sig.port_num   == read_only(me.port_num);
    mon.port_name  == read_only(me.port_name);
    mon.port_num   == read_only(me.port_num);
    mon.parent     == me;
  };

  when ACTIVE ROUTER_IN  sbt_agent_u {
    bfm: sbt_bfm1_u is instance;
    keep all of {
      bfm.port_name  == read_only(me.port_name);
      bfm.port_num   == read_only(me.port_num);
      bfm.parent     == me;
    };
  };

  when ACTIVE ROUTER_OUT sbt_agent_u {
    bfm: sbt_bfm2_u is instance;
    keep all of {
      bfm.port_name  == read_only(me.port_name);
      bfm.port_num   == read_only(me.port_num);
      bfm.parent     == me;
    };
  };
};

extend sbt_bfm1_u {
   -- A reference to the agent that instances the BFM
   parent: sbt_agent_u;
   event clock_e is only @parent.main_clk_e;
};

extend sbt_bfm2_u {
   -- A reference to the agent that instances the BFM
   parent: sbt_agent_u;
   event clock_e is only @parent.main_clk_e;
};

extend sbt_mon_u {
   -- A reference to the agent that instances the monitor
   parent: sbt_agent_u;
   event clock_e is only @parent.main_clk_e;
};

'>

