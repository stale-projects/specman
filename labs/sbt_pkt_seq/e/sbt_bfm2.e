<'
import sbt_bfm2_h;
extend sbt_bfm2_u {

  !valid_iop   : inout simple_port of bit;
  !suspend_iop : inout simple_port of bit;

  some_delay: byte; 
  keep soft some_delay in [1..5];

  run() is also {
      suspend_iop$ = 1;
  };
  drive_bus() @clock_e is only {
      while TRUE {
        sync true(valid_iop$ == 1);
        gen some_delay;
        wait [some_delay];
        suspend_iop$ = 0;  
        while (valid_iop$ == 1) {
          wait [1];
        };
        suspend_iop$ = 1;
      };  
  };

};
'>
