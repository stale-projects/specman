<'
import sbt_sync_h;
extend sbt_sync_u {
  clk_ip: in event_port is instance;
  keep bind(clk_ip, external);
  keep clk_ip.hdl_path() == "clock";
  keep clk_ip.edge()     == fall;

  -- Added reset port to control/monitor reset to the DUT
  reset_iop  : inout simple_port of bit  is instance;
  keep bind(reset_iop,   external);
  keep soft reset_iop.hdl_path()   == "reset";

};
'>
