<'
import sbt_agent_h;
extend sbt_agent_u {

  event reset_start_e;
  on reset_start_e {
    rerun();
  };

  rerun() is also {
    mon.rerun();
  };

  connect_pointers() is also {
    mon.data_iop     = sig.data_iop;
    mon.suspend_iop  = sig.suspend_iop;
    mon.valid_iop    = sig.valid_iop;
    mon.error_iop    = sig.error_iop;
  };

  when ACTIVE ROUTER_IN  sbt_agent_u {
    connect_pointers() is also {
      bfm.data_iop     = sig.data_iop;
      bfm.suspend_iop  = sig.suspend_iop;
      bfm.valid_iop    = sig.valid_iop;
    };
    rerun() is also {
      bfm.rerun();
    };
  };

  when ACTIVE ROUTER_OUT sbt_agent_u {
    connect_pointers() is also {
      bfm.suspend_iop  = sig.suspend_iop;
      bfm.valid_iop    = sig.valid_iop;
    };
    rerun() is also {
      bfm.rerun();
    };
  };

};
'>

