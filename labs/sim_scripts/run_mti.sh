#!/bin/csh -f

set spec='specview'
set lang=""

if ( `echo $0 | grep _verilog_` != "" ) then
  set lang='verilog'
  set stub='specman.v'
endif

if ( `echo $0 | grep _vhdl_`    != "" ) then
  set lang='vhdl'
  set stub='specman_qvh.vhd'
endif

echo "Using language=$lang"

# ====================================================
# Script for Specman training lab with mti       
# ====================================================

setenv DUT ../../dut/$lang

if ( ! -e $DUT/$stub ) then 
  echo "Looks like you did not create the stubs file... exiting."
  exit 1
endif 

# If a modelsim.ini file is present
if ( -e modelsim.ini ) then
  rm modelsim.ini
endif
ln -s ${DUT}/modelsim.ini modelsim.ini

if ( -e work ) then
  rm -rf work
endif

if ( $lang == verilog ) then
  vlib work
  vlog ${DUT}/$stub
  vlog ${DUT}/router.v
  $spec vsim -keepstdout -i top specman specman_wave -pli $SPECMAN_HOME/`sn_arch.sh`/libmti_sn_boot.so
endif 

if ( $lang == vhdl ) then
  vlib work
  vcom ${DUT}/$stub
  vcom -explicit ${DUT}/router.vhd
  $spec vsim -keepstdout -i top
endif

